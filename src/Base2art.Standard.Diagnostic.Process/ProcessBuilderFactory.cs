﻿namespace Base2art.Diagnostic
{
    using System.Diagnostics;
    using System.Text;

    public class ProcessBuilderFactory : IProcessBuilderFactory
    {
        public IProcessBuilder Create() => new ProcessorBuilder();

        private class ProcessorBuilder : ProcessorBuilderBase
        {
            protected override void SetArguments(ProcessStartInfo startInfo, string[] parameters)
            {
                var sb = new StringBuilder();

                foreach (var str in parameters)
                {
                    AppendArgument(sb, str);
                }

                startInfo.Arguments = sb.ToString();
            }

            internal static void AppendArgument(StringBuilder stringBuilder, string argument)
            {
                if (stringBuilder.Length != 0)
                {
                    stringBuilder.Append(' ');
                }

                if (argument.Length != 0 && ContainsNoWhitespaceOrQuotes(argument))
                {
                    stringBuilder.Append(argument);
                }
                else
                {
                    stringBuilder.Append('"');
                    var index = 0;
                    while (index < argument.Length)
                    {
                        var ch = argument[index++];
                        switch (ch)
                        {
                            case '"':
                                stringBuilder.Append('\\');
                                stringBuilder.Append('"');
                                continue;
                            case '\\':
                                var repeatCount = 1;
                                while (index < argument.Length && argument[index] == '\\')
                                {
                                    ++index;
                                    ++repeatCount;
                                }

                                if (index == argument.Length)
                                {
                                    stringBuilder.Append('\\', repeatCount * 2);
                                    continue;
                                }

                                if (argument[index] == '"')
                                {
                                    stringBuilder.Append('\\', repeatCount * 2 + 1);
                                    stringBuilder.Append('"');
                                    ++index;
                                    continue;
                                }

                                stringBuilder.Append('\\', repeatCount);
                                continue;
                            default:
                                stringBuilder.Append(ch);
                                continue;
                        }
                    }

                    stringBuilder.Append('"');
                }
            }

            private static bool ContainsNoWhitespaceOrQuotes(string s)
            {
                foreach (var c in s)
                {
                    if (char.IsWhiteSpace(c) || c == '"')
                    {
                        return false;
                    }
                }

                return true;
            }
        }
    }
}