﻿namespace Base2art.Diagnostic
{
    using System.IO;
    using System.Security;
    using System.Threading.Tasks;

    public interface IProcessBuilder
    {
        IProcessBuilder WithWorkingDirectory(DirectoryInfo dir);
        IProcessBuilder WithWorkingDirectory(string dir);
        IProcessBuilder WithExecutable(string exe);
        IProcessBuilder WithExecutable(FileInfo exe);
        IProcessBuilder WithArguments(string[] parameters);
        IProcessBuilder WithArguments(string arguments);
        IProcessBuilder WithOutputWriter(TextWriter writer);
        IProcessBuilder WithErrorWriter(TextWriter writer);

        IProcessBuilder WithEnvironmentVariable(string name, string value);

        IProcessBuilder WithLoadedUserProfile();
        IProcessBuilder WithLoadedUserProfile(bool loadProfile);

        IProcessBuilder WithUser(string username);
        IProcessBuilder WithPassword(SecureString password);
        IProcessBuilder WithPassword(string password);

        Task<int> Execute();
    }
}