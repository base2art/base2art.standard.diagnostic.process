namespace Base2art.Diagnostic
{
    using System;
    using System.Diagnostics;
    using System.IO;
    using System.Security;
    using System.Threading.Tasks;

    public abstract class ProcessorBuilderBase : IProcessBuilder
    {
        private TextWriter errorWriter;
        private TextWriter outputWriter;

        private readonly TaskCompletionSource<int> tcs = new TaskCompletionSource<int>();
        private Process legacyProcess;

        protected ProcessorBuilderBase()
        {
            this.StartInfo = new ProcessStartInfo();
            this.StartInfo.UseShellExecute = false;
            this.StartInfo.RedirectStandardOutput = true;
            this.StartInfo.RedirectStandardError = true;
        }

        private ProcessStartInfo StartInfo { get; }

        public IProcessBuilder WithWorkingDirectory(DirectoryInfo dir)
        {
            this.StartInfo.WorkingDirectory = dir.FullName;
            return this;
        }

        public IProcessBuilder WithWorkingDirectory(string dir)
        {
            this.StartInfo.WorkingDirectory = dir;
            return this;
        }

        public IProcessBuilder WithExecutable(string exe)

        {
            this.StartInfo.FileName = exe ?? throw new ArgumentNullException(nameof(exe));
            return this;
        }

        public IProcessBuilder WithExecutable(FileInfo exe)
        {
            this.StartInfo.FileName = exe?.FullName ?? throw new ArgumentNullException(nameof(exe));
            return this;
        }

        public IProcessBuilder WithArguments(string[] parameters)
        {
            this.SetArguments(this.StartInfo, parameters);
            return this;
        }

        public IProcessBuilder WithArguments(string arguments)
        {
            this.StartInfo.Arguments = arguments;
            return this;
        }

        public IProcessBuilder WithOutputWriter(TextWriter writer)
        {
            this.outputWriter = writer;
            return this;
        }

        public IProcessBuilder WithErrorWriter(TextWriter writer)
        {
            this.errorWriter = writer;
            return this;
        }

        public IProcessBuilder WithEnvironmentVariable(string name, string value)
        {
            this.StartInfo.Environment[name] = value;
            return this;
        }

        public IProcessBuilder WithLoadedUserProfile()
            => this.WithLoadedUserProfile(true);

        public IProcessBuilder WithLoadedUserProfile(bool loadProfile)
        {
            this.StartInfo.LoadUserProfile = loadProfile;
            return this;
        }

        public IProcessBuilder WithUser(string username)
        {
            this.StartInfo.UserName = username;
            return this;
        }

        public IProcessBuilder WithPassword(SecureString password)
        {
            this.StartInfo.Password = password;
            return this;
        }

        public IProcessBuilder WithPassword(string password)
        {
            if (password == null)
            {
                return this;
            }

            var secure = new SecureString();
            foreach (var c in password)
            {
                secure.AppendChar(c);
            }

            return this.WithPassword(secure);
        }

        public async Task<int> Execute()
        {
            var process = new Process
                          {
                              StartInfo = this.StartInfo,

                              EnableRaisingEvents = true
                          };

            this.legacyProcess = process;
            await this.ExecuteInternal(process);
            await Task.Delay(TimeSpan.FromMilliseconds(2));
            var exitCode = process.ExitCode;

            try
            {
                process.OutputDataReceived -= this.OnProcessOnOutputDataReceived;
                process.ErrorDataReceived -= this.OnProcessOnErrorDataReceived;
                process.Exited -= this.OnProcessOnExited;
            }
            catch (Exception)
            {
            }

            process.Dispose();
            return exitCode;
        }

        private Task<int> ExecuteInternal(Process process)
        {
            process.Exited += this.OnProcessOnExited;

            if (this.outputWriter != null)
            {
                process.OutputDataReceived += this.OnProcessOnOutputDataReceived;
            }

            if (this.errorWriter != null)
            {
                process.ErrorDataReceived += this.OnProcessOnErrorDataReceived;
            }

            var started = process.Start();
            if (!started)
            {
                //you may allow for the process to be re-used (started = false) 
                //but I'm not sure about the guarantees of the Exited event in such a case
                throw new InvalidOperationException("Could not start process: " + process);
            }

            if (this.outputWriter != null)
            {
                process.BeginOutputReadLine();
            }

            if (this.errorWriter != null)
            {
                process.BeginErrorReadLine();
            }

            return tcs.Task;
        }

        private void OnProcessOnExited(object sender, EventArgs args)
        {
            int? legacyProcessExitCode = this.legacyProcess?.ExitCode;
            this.tcs.SetResult(legacyProcessExitCode.GetValueOrDefault());
        }

        private void OnProcessOnErrorDataReceived(object x, DataReceivedEventArgs y)
        {
            this.Write(this.errorWriter, y.Data);
        }

        private void OnProcessOnOutputDataReceived(object x, DataReceivedEventArgs y)
        {
            this.Write(this.outputWriter, y.Data);
        }

        protected abstract void SetArguments(ProcessStartInfo startInfo, string[] parameters);

        private void Write(TextWriter writer, string content)
        {
            if (!string.IsNullOrEmpty(content))
            {
                writer.WriteLine(content.Trim());
            }

            writer.Flush();
        }
    }
}