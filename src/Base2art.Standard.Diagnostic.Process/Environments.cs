namespace Base2art.Diagnostic
{
    using System;
    using System.IO;
    using System.Linq;
    using System.Threading.Tasks;

    public static class Environments
    {
        private static string[] PathParts()
        {
            var path = Environment.GetEnvironmentVariable("PATH") ?? string.Empty;

            var pathParts = path.Split(Path.PathSeparator)
                                .Where(x => !string.IsNullOrWhiteSpace(x))
                                .Select(x => x.Trim()).ToArray();
            return pathParts;
        }

        public static async Task<FileInfo> FindFileInPath(params string[] exeNames)
        {
            var pathParts = PathParts();

            foreach (var pathPart in pathParts)
            {
                foreach (var exeName in exeNames)
                {
                    var next = Path.Combine(pathPart, exeName);
                    var info = new FileInfo(next);
                    if (info.Exists)
                    {
                        if (!info.Attributes.HasFlag(FileAttributes.ReparsePoint))
                        {
                            return info;
                        }

                        var readlink = await FindFileInPath("readlink", "readlink.exe");

                        if (readlink == null)
                        {
                            return info;
                        }

                        var factory = new ProcessBuilderFactory();
                        using (var swo = new StringWriter())
                        {
                            using (var swe = new StringWriter())
                            {
                                var result = await factory.Create()
                                                          .WithExecutable(readlink.FullName)
                                                          .WithArguments(info.FullName)
                                                          .WithOutputWriter(swo)
                                                          .WithErrorWriter(swe)
                                                          .Execute();

                                var newPath = swo.GetStringBuilder().ToString().Trim();

                                var newInfo = new FileInfo(newPath);
                                return newInfo.Exists ? newInfo : info;
                            }
                        }
                    }
                }
            }

            return null;
        }
    }
}