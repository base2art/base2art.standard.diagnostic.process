﻿namespace Base2art.Diagnostic
{
    using System;
    using System.IO;
    using System.Threading.Tasks;

    public static class Processing
    {
        public static Task Execute(
            this IProcessBuilderFactory processorFactory,
            DirectoryInfo workingDir,
            string exe,
            params string[] parameters) => processorFactory.Create()
                                                           .WithWorkingDirectory(workingDir)
                                                           .WithExecutable(exe)
                                                           .WithArguments(parameters)
                                                           .WithOutputWriter(Console.Out)
                                                           .WithErrorWriter(Console.Out)
                                                           .Execute();

        public static Task Execute(
            this IProcessBuilderFactory processorFactory,
            DirectoryInfo workingDir,
            string exe,
            string parameters) => processorFactory.Create()
                                                  .WithWorkingDirectory(workingDir)
                                                  .WithExecutable(exe)
                                                  .WithArguments(parameters)
                                                  .WithOutputWriter(Console.Out)
                                                  .WithErrorWriter(Console.Out)
                                                  .Execute();
    }

    public interface IProcessBuilderFactory
    {
        IProcessBuilder Create();
    }
}