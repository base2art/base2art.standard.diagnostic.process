namespace Base2art.Standard.Diagnostic.Process.Features
{
    using System;
    using System.IO;
    using System.Threading.Tasks;
    using Base2art.Diagnostic;
    using FluentAssertions;
    using Xunit;

    public class UnitTest1
    {
        [Fact]
        public async void Test1()
        {
            var factory = new ProcessBuilderFactory();
            Func<Task> a = () => factory.Create().Execute();
            await a.Should().ThrowAsync<InvalidOperationException>();
        }

        [Fact]
        public async void Test2()
        {
            var path = await Environments.FindFileInPath("dotnet", "dotnet.exe");
            path.FullName.ToUpperInvariant().Should().BeOneOf(
                                                              "/usr/lib64/dotnet/dotnet".ToUpperInvariant(),
                                                              "c:\\Program Files\\dotnet\\dotnet.exe".ToUpperInvariant());
        }

        [Fact]
        public async void Test4()
        {
            using (var sr = new StringWriter())
            {
                var factory = new ProcessBuilderFactory();
                var result = await factory.Create()
                             .WithExecutable("pwsh")
                             .WithArguments(new[] {"-Command", "echo (4*2); exit 45"})
                             .WithOutputWriter(sr)
                             .Execute();

                sr.GetStringBuilder().ToString().Trim().Should().Be("8");
                result.Should().Be(45);
            }
        }

        [Fact]
        public async void Test3()
        {
            using (var sr = new StringWriter())
            {
                var factory = new ProcessBuilderFactory();
                await factory.Create()
                             .WithExecutable("dotnet")
                             .WithArguments(new[] {"--list-runtimes"})
                             .WithOutputWriter(sr)
                             .Execute();
//                sr.Flush();

                sr.GetStringBuilder().ToString().Should().ContainAny("2.2.0", "2.2.1");
            }
        }
    }
}