
$version = Get-Content -Raw ".version"


echo "compiling 1..."
dotnet build --configuration Release -v q

if (-not$?)
{
    exit $LASTEXITCODE
}

echo "compiling 2..."
dotnet build --configuration Release -v q

if (-not$?)
{
    exit $LASTEXITCODE
}



echo "testing..."
$projs = Get-ChildItem -Path "tests"
foreach ($item in $projs)
{
    $csprojs = Get-ChildItem -Path $item.FullName -Filter "*.csproj"

    foreach ($csproj in $csprojs)
    {
        #-v q 
        dotnet test --configuration Release $csproj.FullName

        if (-not$?)
        {
            exit $LASTEXITCODE
        }
    }
}



echo "packing..."
$projs = Get-ChildItem -Path "src"
foreach ($item in $projs)
{
    $csprojs = Get-ChildItem -Path $item.FullName -Filter "*.csproj"

    foreach ($csproj in $csprojs)
    {
        #-v q 
        dotnet pack "$($csproj.FullName)" --include-symbols --include-source  -v q --configuration Release "/p:PackageVersion=$($version)"



        if (-not$?)
        {
            exit $LASTEXITCODE
        }

        $releaseDir = Join-Path "$($csproj.Directory.FullName)" "bin"
        $releaseDir = Join-Path $releaseDir "Release"
        $packages = Get-ChildItem $releaseDir -Filter "*.symbols.nupkg"
        foreach ($pkg in $packages)
        {
            Remove-Item $pkg
        }
    }
}


